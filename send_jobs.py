from datetime import datetime
import os

date = datetime.now().strftime("%Y%m%d_%H%M%S")
HITS_dir="/data/hnl/MC16_HITS/"

submissions = [
    # {'node' : 'u10', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311621.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_5G_lt10dd.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u11', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311621.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_5G_lt10dd.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u12', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311657.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_20G_lt10dd.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u13', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311657.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_20G_lt10dd.simul.HITS.e7422_e5984_a875/"},

    # {'node' : 'u6', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311636.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd_el.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u7', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311636.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt10dd_el.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u8', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.312987.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_ee.simul.HITS.e7902_e5984_a875/"},
    # {'node' : 'u9', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.312987.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_ee.simul.HITS.e7902_e5984_a875/"},

    # {'node' : 'u21', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.312990.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_emu.simul.HITS.e7902_e5984_a875/"},
    # {'node' : 'u22', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.312990.Pythia8EvtGen_A14NNPDF23LO_WelHNL50_10G_lt10dd_emu.simul.HITS.e7902_e5984_a875/"},

    # {'node' : 'u15', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311635.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt1dd_el.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u17', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311635.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt1dd_el.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u19', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311637.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd_el.simul.HITS.e7660_e5984_a875/"},
    # {'node' : 'u20', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311637.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd_el.simul.HITS.e7660_e5984_a875/"},
    # {'node' : 'u12', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311632.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt1dd.simul.HITS.e7422_e5984_a875/"},
    # {'node' : 'u14', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311632.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt1dd.simul.HITS.e7422_e5984_a875/"},
    
    {'node' : 'u10', 'reco_type': "AOD"        , 'sample': "mc16_13TeV.311634.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd.simul.HITS.e7422_e5984_a875/"},
    {'node' : 'u11', 'reco_type': "DAOD_RPVLL" , 'sample': "mc16_13TeV.311634.Pythia8EvtGen_A14NNPDF23LO_WmuHNL50_10G_lt100dd.simul.HITS.e7422_e5984_a875/"},

]

dry_run = False

for submission in submissions:
    (_, _, phys_name, _, _, _) = submission['sample'].split('.')
    name = "_".join(phys_name.split("_")[2:])

    for i in range(1,51):
        print i
        out_dir = "/data/hnl/filter_test/output/batch/{}/{}/{}/{}".format(name, submission['reco_type'], date, i)
        
        if not dry_run:
            os.makedirs(out_dir)

        command = "qsub -o /home/newhouse/public/Analysis/HNL/recon/batch_logs/ -e /home/newhouse/public/Analysis/HNL/recon/batch_logs/ -l walltime=144:00:00 -l nodes={}:ppn=1 -V -v outputDir='{}',MYNO={},HITSDIR='{}' jobfile_{}.pbs".format(submission['node'], out_dir, i, HITS_dir + submission['sample'], submission['reco_type'])
        if dry_run:
            command = "echo " + command

        os.system(command)
        print(out_dir)
